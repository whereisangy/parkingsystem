# Parking Management System
This system allows users to book parking spots in advance for themselves
. When the spot is busy, one can not make the reservation for that respective
 spot. The reservation can be made atleast for 10 minutes and utmost for 30
  minutes
 for
 atleast 10
 minutes and
 atmost 30 minutes.

## Instructions to execute:
load initital data:

$python manage.py create_spots  

run docker:

$docker-compose up  

## Further Customisations
Login for Bob(or any Operator(s)) to reserve parking spots for the customers.

Filter free/busy spots from the list.

Possibility to add multiple spots.

Visual representation of the bookings per spot (duration)

Fixtures could be added instead of management command