from django.conf.urls import url

from .views import home, reserve_spot

urlpatterns = [
    url(r'^$', home, name='home'),
    url(r'^reserve/(\d+)/$', reserve_spot, name='reserve')
]
