from django.core.management.base import BaseCommand

from parkingapp.models import ParkingSpot


class Command(BaseCommand):

    help = 'create dummy parking spots'

    def handle(self, *args, **options):
        ParkingSpot.objects.bulk_create([ParkingSpot(spot_number='SA1'),
                                         ParkingSpot(spot_number='SA2'),
                                         ParkingSpot(spot_number='SB1'),
                                         ParkingSpot(spot_number='SB2')])
