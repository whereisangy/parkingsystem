from django.db import models


class ParkingSpot(models.Model):
    spot_number = models.CharField(max_length=10)
    duration = models.PositiveIntegerField(blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    status = models.CharField(max_length=10, default='Free')

    def __str__(self):
        return self.spot_number


class Customer(models.Model):
    name = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField()
    spot = models.ForeignKey(ParkingSpot, on_delete=models.CASCADE)

    def __str__(self):
        return self.email
