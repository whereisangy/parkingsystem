from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from django import forms

from parkingapp.models import Customer


class ReserveSlotForm(forms.ModelForm):
    duration = forms.IntegerField(min_value=10,
                                  max_value=30,
                                  label='',
                                  help_text='Minimum for 10 minutes and '
                                            'maximum for 30 minutes')
    start_time = forms.TimeField(required=False,
                                 help_text='Leave empty to start now',
                                 label='',
                                 widget=forms.TimeInput(
                                     attrs={'type': 'time'})
                                 )

    class Meta:
        model = Customer
        fields = ('name', 'email',)
        labels = {
            'name': '',
            'email': '',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = 'Name'
        self.fields['email'].widget.attrs['placeholder'] = 'Email'
        self.fields['duration'].widget.attrs['placeholder'] = 'Duration ' \
                                                              'in minutes'

        # Crispy form helper
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Submit',
                                     css_class='btn-primary'))
        self.helper.form_method = 'POST'
