from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.decorators.csrf import csrf_protect

from parkingapp.forms import ReserveSlotForm
from .models import ParkingSpot, Customer


def home(request):
    # get all the free slots
    free_slots = ParkingSpot.objects.all()

    # update reserved slots when end time exceeds current time
    free_slots.filter(status='Busy',
                      end_time__lt=timezone.now()
                      ).update(status='Free', duration=None)

    return render(request, 'home.html', {'spots': free_slots})


@csrf_protect
def reserve_spot(request, spot_id):
    spot = ParkingSpot.objects.get(id=spot_id)
    if request.method == 'POST':
        form = ReserveSlotForm(request.POST)
        if form.is_valid():
            spot.duration = form.cleaned_data['duration']

            # default start time is now
            if not form.cleaned_data['start_time']:
                spot.start_time = timezone.now()
            else:
                spot.start_time = form.cleaned_data['start_time']

            spot.end_time = spot.start_time + timezone.timedelta(
                minutes=form.cleaned_data['duration'])

            # spot is busy if the begin time + duration is less than time
            # right now
            if spot.end_time > timezone.now():
                spot.status = 'Busy'

            spot.save()

            # create customer for this spot
            Customer.objects.create(name=form.cleaned_data['name'],
                                    email=form.cleaned_data['email'],
                                    spot=spot)
            return redirect('home')
    else:
        form = ReserveSlotForm()

    return render(request, 'reserve.html', {
        'form': form,
        'spot': spot
    })
