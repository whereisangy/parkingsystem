FROM python:3.6.8
MAINTAINER angy@localhost

COPY ./ /toogethr

WORKDIR /toogethr

RUN pip install -r requirements.txt

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]